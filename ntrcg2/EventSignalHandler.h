/**
* @file
* @ingroup group_subnet_triggers_ntrcg2
*/
/***********************************************************

File Name :
        EventSignalHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_EVENT_SIG messages.


Creation Date:
        31 July 2000

Modification History:
        2013/03/21 Pete Lombard: Major changes and rename to ntrcg2.


Usage Notes:


**********************************************************/

#ifndef event_signal_handler_H
#define event_signal_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Subnet Trigger message handler
void handleEventSigMsg(Message &m, void *arg);


#endif
