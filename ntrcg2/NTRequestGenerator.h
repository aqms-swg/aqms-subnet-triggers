/**
* @file
* @ingroup group_subnet_triggers_ntrcg2
*/
/***********************************************************

File Name :
        NTRequestGenerator.h

Original Author:
        Patrick Small

Description:


Creation Date:
        4 August 2000

Modification History:
        2013/03/21 Pete Lombard: Major changes and rename to ntrcg2.


Usage Notes:


**********************************************************/

#ifndef nt_request_gen_H
#define nt_request_gen_H

// Various include files
#include <string>
#include <map>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Connection.h"
#include "RTApplication.h"
#include "DatabaseNTRCG.h"
#include "ChannelReaderDB.h"
#include "EventArchiver.h"
#include "SelectInfo.h"
#include "LockFile.h"


// Definition of a mapping between a stream type and a set of channels
typedef std::map<std::string, ChannelNameList, std::less<std::string>, 
		std::allocator<std::pair<const std::string, ChannelNameList> > > ChannelNameMap;


// Definition of a mapping between one channel and a set of channels
typedef std::map<Channel, ChannelSet, std::less<Channel>, 
		std::allocator<std::pair<const Channel, ChannelSet> > > ChannelMap;


// Definition of a mapping between one channel and a time window
typedef std::map<Channel, SelectInfo, std::less<Channel>, 
		std::allocator<std::pair<const Channel, SelectInfo> > > SelectedList;

// Definition of an Event ID list
typedef std::multimap<TimeStamp, unsigned long, std::less<TimeStamp>, 
	std::allocator<std::pair<const TimeStamp, unsigned long> > > EventList;

class NTRequestGenerator : public RTApplication
{
 private:
    char auth[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char eventsubject[MAXSTR];
    char assocsubject[MAXSTR];
    char sigsubject[MAXSTR];
    char configdir[MAXSTR];
    char eventlogdir[MAXSTR];

    double includeAllMag;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    char lock_file_name[MAXSTR];
    LockFile *lock;

    char archive_directory[MAXSTR];
    double wfRetryInterval;
    double wfMaxRetry;
    EventList events;
    EventArchiver old_events;
    
    ChannelMap chanmap;

    int _LoadConfig(const char *config, ChannelNameMap &cl);
    int _CreateMap(ChannelConfigList &configchans, ChannelSet &active);
    int _LoadChannels();
    int _GetDatabase(DatabaseNTRCG &db);
    int _SendRequestSignal(unsigned long evid);
    int _FindRequestCards(NetworkTrigger &trig, RequestList &rl);
    int _DumpRequests(NetworkTrigger &trig, RequestList &rl);
    int _DumpSummary(unsigned int evid, NetworkTrigger &trig, 
		     RequestList &rl);
    void _dumpQueue();
    
 protected:

 public:
    static ChannelNameList seedchanlist;

    NTRequestGenerator();
    ~NTRequestGenerator();

    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();

    int DoEvent(unsigned long evid, int isAssocEvent);
    char* GetAssocSubject();
};

#endif
