/**
* @file
* @ingroup group_network-triggers_ntrcg2
*/
/***********************************************************

File Name :
        Main.C

Original Author:
        Patrick Small

Description:


Creation Date:
        31 July 2000


Modification History:
Date                 Version           Author                Comment
---------------------------------------------------------------------------
2013/03/21            0.0.1             Pete Lombard: Major changes and rename to ntrcg2.
2015/04/22            0.0.2             Pete Lombard: Change for splitting 
2019/11/25	      0.1.0             Paul Friberg: postgres port
	EVENT_LOCAL into EVENT_QUAKE and GTYPE_LOCAL


Usage Notes:


**********************************************************/

// Various include files
#include <iostream>
#include "RetCodes.h"
#include "RTRuntime.h"
#include "NTRequestGenerator.h"

const char* version = "0.1.0 2019-11-25";
#ifdef USE_POSTGRES
const char* db_version = "Using Postgres SQL syntax";
#else
const char* db_version = "Using Oracle SQL syntax";
#endif


// Displays program usage information to the user
void usage(char *progname)
{
  std::cout << std::endl << "usage: " << progname << " <config file>" << std::endl;
  std::cout << "Version " << version << std::endl << std::endl;
  std::cout << "DBVersion " << db_version << std::endl << std::endl;
  exit(0);
}


int main(int argc, char **argv)
{
  NTRequestGenerator rcg;
  rcg.SetProgramVersion(version);

  // Check for correct number of arguments
  if (argc != 2) {
    usage(argv[0]);
    return(1);
  }

  RTRuntime run(&rcg, argv[1]);
  if (!run) {
    std::cout << "Error (main): Unable to create runtime environment" << std::endl;
    return(1);
  }
  if (run.Run() != TN_SUCCESS) {
    std::cout << "Error (main): Run failure" << std::endl;
  }

  std::cout << "Terminating program" << std::endl;
  return(0);
}

