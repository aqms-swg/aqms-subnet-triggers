/**
* @file
* @ingroup group_subnet_triggers_ntrcg2
*/
/***********************************************************

File Name :
        SelectInfo.h

Original Author:
        Patrick Small

Description:


Creation Date:
        14 June 1999

Modification History:
        2013/03/21 Pete Lombard: Major changes and rename to ntrcg2.


Usage Notes:


**********************************************************/

#ifndef select_info_H
#define select_info_H

// Various include files
#include "RequestCard.h"
#include "TimeWindow.h"


class SelectInfo
{
 private:

 protected:

 public:

    TimeWindow win;
    requestPrioType priority;

    SelectInfo();
    SelectInfo(const SelectInfo &s);
    ~SelectInfo();

    SelectInfo& operator=(const SelectInfo &s);
};


#endif
