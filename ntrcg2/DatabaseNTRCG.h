/**
* @file
* @ingroup group_subnet_triggers_ntrcg2
*/
/***********************************************************

File Name :
        DatabaseNTRCG.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
        2013/03/21 Pete Lombard: Major changes and rename to ntrcg2.


Usage Notes:


**********************************************************/

#ifndef database_trig2db_H
#define database_trig2db_H

// Various include files
#include <list>
#include <map>
#include <set>
#include <string>
#include "Database.h"
#include "NetworkTrigger.h"
#include "RequestCard.h"

using namespace std;

// Definition of the request card list
typedef list<RequestCard, allocator<RequestCard> > RequestList;

// Definition of a channel identifier list
typedef set<string, less<string>, allocator<string> > ChannelNameList;

typedef list<Channel> ChannelList;

typedef map<string,ChannelList> StationChannelsMap;

class DatabaseNTRCG : public Database
{
 private:
  static StationChannelsMap stationcache;
  
  int _GetStationTriggers(NetworkTrigger &trig);

 protected:

 public:
    DatabaseNTRCG();
    DatabaseNTRCG(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseNTRCG();

    int DoGenerateCards();

    int GetTrigger(unsigned int evid, NetworkTrigger &trig);
    int GetEventMag(unsigned int evid, double &mag);
    int GetWFSNCLs(unsigned int evid, ChannelList &wfList);
    int WriteRequests(unsigned long evid, const char *network,
		      const char *source, RequestList &rl);

};


#endif
