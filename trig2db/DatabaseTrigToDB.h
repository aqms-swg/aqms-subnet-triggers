/**
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file for DatabaseTrigToDB.C
 */
/***********************************************************

File Name :
        DatabaseTrigToDB.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef database_trig2db_H
#define database_trig2db_H

// Various include files
#include "Database.h"
#include "NetworkTrigger.h"

class DatabaseTrigToDB : public Database
{
 private:

    int _WriteTriggers(NetworkTrigger &t);

 protected:

 public:
    DatabaseTrigToDB();
    DatabaseTrigToDB(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTrigToDB();

    int WriteTrigger(NetworkTrigger &t, bool useSubnetCode); /**< Save to nettrig */
};

#endif
