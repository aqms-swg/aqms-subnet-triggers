/**
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file for TrigToDB.C
 */
/***********************************************************

File Name :
        TrigToDB.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
		  09 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef trig2db_H
#define trig2db_H

// Various include files
#include <stdint.h>
#include "GenLimits.h"
#include "Connection.h"
#include "RTApplication.h"
#include "NetworkTrigger.h"
#include "LockFile.h"


class TrigToDB : public RTApplication
{

 private:
    char trigsubject[MAXSTR];
    char sigsubject[MAXSTR];
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    char lock_file_name[MAXSTR];
    LockFile *lock;
    bool useSubnetCode;

    int _SendTriggerSignal(uint32_t trigid);

 protected:

 public:
    TrigToDB();
    ~TrigToDB();

    int ParseConfiguration(const char *tag, const char *value); /**< read configuration file */
    int Startup();  /**< Initialize trig2db, including setting up handlers for CMS messages */
    int Run(); /**< Start the program */
    int Shutdown(); /**< Perform clean up and shutdown */

    int DoTrigger(NetworkTrigger &trig); /**< Associate provided trigger with candidate events */
};

#endif
