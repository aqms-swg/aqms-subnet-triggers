/**
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file for NetworkTrigHandler.C
 */
/***********************************************************

File Name :
        NetworkTrigHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_SUBNET_TRIG messages.


Creation Date:
        28 July 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef network_trig_handler_H
#define network_trig_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Subnet Trigger message handler
void handleNetworkTrigMsg(Message &m, void *arg);


#endif
