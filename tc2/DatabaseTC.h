/**
* @file
* @ingroup group_subnet_triggers_tc2
*/
/***********************************************************

File Name :
        DatabaseTC.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	2013/03/21 Pete Lombard: Major changes and rename to tc2 


Usage Notes:


**********************************************************/

#ifndef database_trig2db_H
#define database_trig2db_H

// Various include files
#include "Database.h"
#include "Event.h"
#include "NetworkTrigger.h"


class DatabaseTC : public Database
{
 private:

    int _IsPreferred();
    int _GetOrigin(uint32_t orid, Origin &org);
    int _WriteOrigin(Event &e, uint32_t &prefor);
    int _GetStationTriggers(NetworkTrigger &trig);
    int _PromoteTrigs(Event &e, NetworkTrigger &trig, uint32_t &prefor);
    
 protected:

 public:
    DatabaseTC();
    DatabaseTC(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTC();

    int GetNextEventID(uint32_t&evid);
    int GetEvent(Event &e);
    int GetTrigger(NetworkTrigger &trig);
    int GetUnmatchedCount(uint32_t &evid, uint32_t &trigid,
			  int &trigCount, int &unmatchedCount);
    
    int WriteEvent(Event &e, NetworkTrigger &t, int promoteTrigs);
    int AssocEventTrigger(Event &e, NetworkTrigger &t, char *network,
			  char *source, int wfflag);
};


#endif
