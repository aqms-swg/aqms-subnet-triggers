/**
* @file
* @ingroup group_subnet_triggers_tc2
*/
/***********************************************************

File Name :
        TrigCoord.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	2013/03/21 Pete Lombard: Major changes and rename to tc2 


Usage Notes:


**********************************************************/

#ifndef trig2db_H
#define trig2db_H

// Various include files
#include <map>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Connection.h"
#include "RTApplication.h"
#include "EventAssoc.h"
#include "DatabaseTC.h"
#include "LockFile.h"
#include "Tlay.h"

// Definition of the event list
typedef std::map<TimeStamp, EventAssoc, std::less<TimeStamp>, 
	std::allocator<std::pair<const TimeStamp, EventAssoc> > > EventList;


class TrigCoord : public RTApplication
{

 private:
    char auth[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char trigsubject[MAXSTR];
    char eventsubject[MAXSTR];
    char assocsigsubject[MAXSTR];
    char unassoctrigsigsubject[MAXSTR];
    Duration assocdur;
    Duration maxtrigdur;
    Duration ecfinaldur;
    Duration maxprocdur;
    Duration pretrigbuf;
    Duration ttPreP;
    Duration ttPostS;
    double ttGoodMatchPct;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    int promoteTrigs;
    char velModelFile[MAXSTR];
    char lock_file_name[MAXSTR];
    LockFile *lock;
    Tlay tlay;

    EventList events;
    
    int _GetDatabase(DatabaseTC &db);
    int _CreateEvent(Event &e, NetworkTrigger &trig);
    int _GetEvent(Event &e);
    int _GetTrigger(NetworkTrigger &t);
    int _WriteAssocs(Event &e, NetworkTrigger &trig, int wfflag);
    int _SendAssocEventSignal(unsigned long evid);
    int _SendUnassocTrigSignal(unsigned long evid);
    double _GetTTMatchPct(Event &e, NetworkTrigger &t);
    void _dumpQueue();
    
 protected:

 public:
    TrigCoord();
    ~TrigCoord();

    int ParseConfiguration(const char *tag, const char *value);
    int Startup();
    int Run();
    int Shutdown();

    int DoEvent(unsigned long evid);
    int DoTrigger(unsigned long trigid);
    
};

#endif
