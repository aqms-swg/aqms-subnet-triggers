/**
* @file
* @ingroup group_subnet_triggers_tc2
*/
/***********************************************************

File Name :
        TrigCoord.C

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	2013/03/21 Pete Lombard: Major changes and rename to tc2 

Usage Notes:


**********************************************************/

// Various include files
#include <iostream>
#include <cstring>
#include "RetCodes.h"
#include "Compat.h"
#include "SeismoFuncs.h"
#include "TrigCoord.h"
#include "DatabaseTC.h"
#include "EventSignalHandler.h"
#include "TriggerSignalHandler.h"

// Constants for required config file settings
const int AUTH                   = 0x000001;
const int SUBSOURCE              = 0x000002;
const int TRIGSUBJECT            = 0x000004;
const int EVENTSUBJECT           = 0x000008;
const int ASSOCSIGSUBJECT        = 0x000010;
const int UNASSOCTRIGSIGSUBJECT  = 0x000020;
const int ASSOCDUR               = 0x000040;
const int MAXTRIGDUR             = 0x000080;
const int ECFINALDUR             = 0x000100;
const int MAXPROCDUR             = 0x000200;
const int PRETRIGBUF             = 0x000400;
const int VELMODELFILE           = 0x000800;
const int TTPREP                 = 0x001000;
const int TTPOSTS                = 0x002000;
const int TTGOODMATCH            = 0x004000;
const int DBSERVICE              = 0x008000;
const int DBUSER                 = 0x010000;
const int DBPASSWORD             = 0x020000;
const int DBINTERVAL             = 0x040000;
const int DBRETRIES              = 0x080000;
const int LOCKFILE               = 0x100000;



TrigCoord::TrigCoord()
{
    strcpy(auth, "");
    strcpy(subsource, "");
    strcpy(trigsubject, "");
    strcpy(eventsubject, "");
    strcpy(assocsigsubject, "");
    strcpy(unassoctrigsigsubject, "");
    strcpy(dbservice, "");
    strcpy(dbuser, "");
    strcpy(dbpass, "");
    strcpy(velModelFile, "");
    ttPreP = 0.0;
    ttPostS = 0.0;
    ttGoodMatchPct = 0.0;
    promoteTrigs = TN_FALSE;
    dbinterval = 0;
    dbretries = 0;
    checklist = 0;
}



TrigCoord::~TrigCoord()
{
}



int TrigCoord::_GetDatabase(DatabaseTC &db)
{
    if (!db) {
	sm.ErrorMessage("(TrigCoord::_GetDatabase): Unable to open database connection");
	return(TN_FAILURE);
    }
    if (db.SetRetryInterval(dbinterval) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetDatabase): Unable to set db retry interval");
	return(TN_FAILURE);
    }
    if (db.SetNumRetries(dbretries) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetDatabase): Unable to set number of db retries");
	return(TN_FAILURE);
    }
    if (!(db.IsConnected())) {
	sm.ErrorMessage("(TrigCoord::_GetDatabase): Unable to connect to database");
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_CreateEvent(Event &e, NetworkTrigger &trig)
{
    Origin org;
    DatabaseTC db(dbservice, dbuser, dbpass);

    // Get an instance of the database
    if (_GetDatabase(db) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_CreateEvent): Unable to get database");
	return(TN_FAILURE);
    }

    // Populate event information with trigger data
    e.etype = EVENT_SUBTRIG;
    strcpy(e.source, auth);
    strcpy(e.subsource, subsource);

    // Populate the origin information with trigger data
    strcpy(org.locevid, trig.locid);
    strcpy(org.algorithm, trig.algorithm);
    strcpy(org.source, trig.source);
    strcpy(org.subsource, trig.subsource);
    org.origdate = trig.trigdate;
    org.latitude = 0.0;
    org.longitude = 0.0;
    org.depth = 0.0;
    org.bogus = TN_TRUE;

    e.origins.insert(e.origins.begin(), org);

    // Get the next event identifier
    if (db.GetNextEventID(e.evid) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_CreateEvent): Unable to get next event identifier");
	return(TN_FAILURE);
    }

    sm.InfoMessage(Compat::Form("Creating new event %ld for trigger %ld",
				e.evid, trig.trigid));
    sm << e;

    // Write the new event to the database
    sm.InfoMessage(Compat::Form("Writing new event %ld to database", e.evid));
    if (db.WriteEvent(e, trig, promoteTrigs) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_CreateEvent): Unable to create event");
	return(TN_FAILURE);
    }

    // Associate it with this trigger
    sm.InfoMessage("Associating event with trigger");
    if (db.AssocEventTrigger(e, trig, auth, subsource, TN_TRUE) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_CreateEvent): Unable to associate event with trigger");
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_GetEvent(Event &e)
{
    DatabaseTC db(dbservice, dbuser, dbpass);
    sm.InfoMessage("Retrieving event from the database");

    // Get an instance of the database
    if (_GetDatabase(db) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetEvent): Unable to get database");
	return(TN_FAILURE);
    }

    // Retrieve the event from the database
    if (db.GetEvent(e) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetEvent): Unable to retrieve event from the database");
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_GetTrigger(NetworkTrigger &t)
{
    DatabaseTC db(dbservice, dbuser, dbpass);
    sm.InfoMessage("Retrieving trigger from the database");

    // Get an instance of the database
    if (_GetDatabase(db) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetTrigger): Unable to get database");
	return(TN_FAILURE);
    }

    // Retrieve the trigger from the database
    if (db.GetTrigger(t) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_GetTrigger): Unable to retrieve trigger from the database");
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_WriteAssocs(Event &e, NetworkTrigger &trig, int wfflag)
{
    DatabaseTC db(dbservice, dbuser, dbpass);
    sm.InfoMessage("Writing association into the database");

    // Get an instance of the database
    if (_GetDatabase(db) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_WriteAssocs): Unable to get database");
	return(TN_FAILURE);
    }

    // Associate the event with the trigger
    sm.InfoMessage("Associating event with trigger");
    if (db.AssocEventTrigger(e, trig, auth, subsource, wfflag) != TN_SUCCESS) {
	sm.ErrorMessage(Compat::Form("(TrigCoord::_WriteAssocs): Unable to associate event %d with trigger %d",
				     e.evid, trig.trigid));
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_SendAssocEventSignal(unsigned long evid)
{
    Message msg(TN_TMT_EVENT_SIG);

    sm.InfoMessage(Compat::Form("Sending associated event signal for %ld", 
				evid));

    if (msg.append((int)evid) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendAssocEventSignal): Unable to append event id");
	return(TN_FAILURE);
    }
    if (msg.setDest(assocsigsubject) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendAssocEventSignal): Unable to set destination subject");
	return(TN_FAILURE);
    }
    if (conn.send(msg) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendAssocEventSignal): Unable to send signal message");
	return(TN_FAILURE);
    }
    if (conn.flush() != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendAssocEventSignal): Unable to flush outgoing messages");
	return(TN_FAILURE);
    }

    return(TN_SUCCESS);
}



int TrigCoord::_SendUnassocTrigSignal(unsigned long evid)
{
    Message msg(TN_TMT_EVENT_SIG);

    sm.InfoMessage(Compat::Form("Sending unassociated trigger signal for %ld",
				evid));

    if (msg.append((int)evid) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendUnassocTrigSignal): Unable to append event id");
	return(TN_FAILURE);
    }
    if (msg.setDest(unassoctrigsigsubject) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendUnassocTrigSignal): Unable to set destination subject");
	return(TN_FAILURE);
    }
    if (conn.send(msg) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendUnassocTrigSignal): Unable to send signal message");
	return(TN_FAILURE);
    }
    if (conn.flush() != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::_SendUnassocTrigSignal): Unable to flush outgoing messages");
	return(TN_FAILURE);
    }
    return(TN_SUCCESS);
}



int TrigCoord::ParseConfiguration(const char *tag, const char *value)
{
    if (strcmp(tag, "Auth") == 0) {
	if (strlen(value) > DB_MAX_AUTH_LEN) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Auth too long - "
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(auth, value);
	    checklist |= AUTH;
	}
    } else if (strcmp(tag, "Subsource") == 0) {
	if (strlen(value) > DB_MAX_SUBSOURCE_LEN) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Subsource too long - "
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(subsource, value);
	    checklist |= SUBSOURCE;
	}
    } else if (strcmp(tag, "TriggerSubject") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << 
		"Error (TrigCoord::ParseConfiguration): TriggerSubject too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(trigsubject, value);
	    checklist |= TRIGSUBJECT;
	}
    } else if (strcmp(tag, "EventSubject") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << 
		"Error (TrigCoord::ParseConfiguration): EventSubject too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(eventsubject, value);
	    checklist |= EVENTSUBJECT;
	}
    } else if (strcmp(tag, "AssocEventSubject") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << 
		"Error (TrigCoord::ParseConfiguration): AssocEventSubject too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(assocsigsubject, value);
	    checklist |= ASSOCSIGSUBJECT;
	}

    } else if (strcmp(tag, "UnassocTrigSubject") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): UnassocTrigSubject too long - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(unassoctrigsigsubject, value);
	    checklist |= UNASSOCTRIGSIGSUBJECT;
	}
    } else if (strcmp(tag, "AssocDuration") == 0) {
	assocdur = Duration((double)atoi(value));
	if ((double)assocdur <= 0.0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for AssocDuration - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= ASSOCDUR;
	}
    } else if (strcmp(tag, "MaxTrigDuration") == 0) {
	maxtrigdur = Duration((double)atoi(value));
	if ((double)maxtrigdur <= 0.0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for MaxTrigDuration - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= MAXTRIGDUR;
	}
    } else if (strcmp(tag, "ECFinalDuration") == 0) {
	ecfinaldur = Duration((double)atoi(value));
	if ((double)ecfinaldur <= 0.0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for ECFinalDuration - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= ECFINALDUR;
	}
    } else if (strcmp(tag, "MaxProcDuration") == 0) {
	maxprocdur = Duration((double)atoi(value));
	if ((double)maxprocdur <= 0.0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for MaxProcDuration - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= MAXPROCDUR;
	}
    } else if (strcmp(tag, "PreTriggerBuffer") == 0) {
	pretrigbuf = Duration((double)atoi(value));
	checklist |= PRETRIGBUF;
    } else if (strcmp(tag, "DBService") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << "Error TrigCoord::ParseConfiguration): DBService too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(dbservice, value);
	    checklist |= DBSERVICE;
	}
    } else if (strcmp(tag, "DBUser") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): DBUser too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(dbuser, value);
	    checklist |= DBUSER;
	}
    } else if (strcmp(tag, "DBPasswd") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): DBPasswd too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(dbpass, value);
	    checklist |= DBPASSWORD;
	}
    } else if (strcmp(tag, "DBConnectionRetryInterval") == 0) {
	dbinterval = atoi(value);
	if (dbinterval <= 0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for DBConnectionRetryInterval - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= DBINTERVAL;
	}
    } else if (strcmp(tag, "DBMaxConnectionRetries") == 0) {
	dbretries = atoi(value);
	if (dbretries <= 0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Invalid value for DBMaxConnectionRetries - " << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    checklist |= DBRETRIES;
	}
    } else if (strcmp(tag, "LockFile") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): LockFile filename too long - "
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(lock_file_name, value);
	    checklist |= LOCKFILE;
	}
    } else if (strcmp(tag, "ttGoodMatchPercent") == 0) {
	ttGoodMatchPct = atof(value);
	if (ttGoodMatchPct < 0.0 || ttGoodMatchPct > 100.0) {
	    std::cout << "Error (TrigCoord::ParseConfiguration): Bad value for GoodMatchPercent; should be between 0 and 100" << std::endl;
	    return(TN_FAILURE);
	}
	checklist |= TTGOODMATCH;
    } else if (strcmp(tag, "VelocityModelFile") == 0) {
	if (strlen(value) > MAXSTR) {
	    std::cout << 
		"Error (TrigCoord::ParseConfiguration): VelocityModelFile too long - " 
		      << value << std::endl;
	    return(TN_FAILURE);
	} else {
	    strcpy(velModelFile, value);
	    checklist |= VELMODELFILE;
	}
    } else if (strcmp(tag, "ttPreP") == 0) {
	ttPreP = atof(value);
	checklist |= TTPREP;
    }
    else if (strcmp(tag, "ttPostS") == 0) {
	ttPostS = atof(value);
	checklist |= TTPOSTS;
    }
    // optional: promoteTriggersToArrivals
    else if (strcmp(tag, "promoteTriggersToArrivals") == 0) {
	if (atoi(value) > 0) {
	    promoteTrigs = TN_TRUE;
	}
    }else {
	std::cout << "Error (TrigCoord::ParseConfiguration): Unrecognized config line" 
		  << std::endl;
	return(TN_FAILURE);
    }
    return(TN_SUCCESS);
}



int TrigCoord::Startup()
{
    char uniqueid[MAXSTR];
    char hostname[MAXSTR];

    sm.InfoMessage("(TrigCoord::Startup) Performing startup...");

    if ((checklist & AUTH) != AUTH) {
	sm.ErrorMessage("(TrigCoord::Startup): Auth not specified");
	return(TN_FAILURE);
    }
    if ((checklist & SUBSOURCE) != SUBSOURCE) {
	sm.ErrorMessage("(TrigCoord::Startup): Subsource not specified");
	return(TN_FAILURE);
    }
    if ((checklist & TRIGSUBJECT) != TRIGSUBJECT) {
	sm.ErrorMessage("(TrigCoord::Startup): TriggerSubject not specified");
	return(TN_FAILURE);
    }
    if ((checklist & EVENTSUBJECT) != EVENTSUBJECT) {
	sm.ErrorMessage("(TrigCoord::Startup): EventSubject not specified");
	return(TN_FAILURE);
    }
    if ((checklist & ASSOCSIGSUBJECT) != ASSOCSIGSUBJECT) {
	sm.ErrorMessage("(TrigCoord::Startup): AssocEventSubject not specified");
	return(TN_FAILURE);
    }
    if ((checklist & UNASSOCTRIGSIGSUBJECT) != UNASSOCTRIGSIGSUBJECT) {
	sm.InfoMessage("(TrigCoord::Startup): UnassocTrigSubject not specified"
		       " using AssocSigSubject instead");
	strcpy(unassoctrigsigsubject, assocsigsubject);
    }
    if ((checklist & ASSOCDUR) != ASSOCDUR) {
	sm.ErrorMessage("(TrigCoord::Startup): AssocDuration configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & MAXTRIGDUR) != MAXTRIGDUR) {
	sm.ErrorMessage("(TrigCoord::Startup): MaxTrigDuration configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & ECFINALDUR) != ECFINALDUR) {
	sm.ErrorMessage("(TrigCoord::Startup): ECFinalDuration configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & MAXPROCDUR) != MAXPROCDUR) {
	sm.ErrorMessage("(TrigCoord::Startup): MaxProcDuration configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & PRETRIGBUF) != PRETRIGBUF) {
	sm.ErrorMessage("(TrigCoord::Startup): PreTriggerBuffer configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & VELMODELFILE ) != VELMODELFILE) {
	sm.ErrorMessage("(TrigCoord::Startup): VelocityModelFile configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & TTPREP) != TTPREP) {
	sm.ErrorMessage("(TrigCoord::Startup): ttPreP configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & TTPOSTS) != TTPOSTS) {
	sm.ErrorMessage("(TrigCoord::Startup): ttPostS configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & TTGOODMATCH) != TTGOODMATCH) {
	sm.ErrorMessage("(TrigCoord::Startup): ttGoodMatchPct configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & DBSERVICE) != DBSERVICE) {
	sm.ErrorMessage("(TrigCoord::Startup): DBService configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & DBUSER) != DBUSER) {
	sm.ErrorMessage("(TrigCoord::Startup): DBUser configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & DBPASSWORD) != DBPASSWORD) {
	sm.ErrorMessage("(TrigCoord::Startup): DBPasswd configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & DBINTERVAL) != DBINTERVAL) {
	sm.ErrorMessage("(TrigCoord::Startup): DBConnectionRetryInterval configuration item not specified");
	return(TN_FAILURE);
    }
    if ((checklist & DBRETRIES) != DBRETRIES) {
	sm.ErrorMessage("(TrigCoord::Startup): DBMaxConnectionRetries configuration item not specified");
    }
    if ((checklist & LOCKFILE) != LOCKFILE) {
	sm.ErrorMessage("(TrigCoord::Startup): LockFile file configuration item not specified");
	return(TN_FAILURE);
    }

    // attempt to lock the file
    lock = new LockFile(lock_file_name);
    if (lock->isValid()) {
	if(lock->lock() == -1) {
	    sm.ErrorMessage("(TrigCoord::Startup): Unable to lock LockFile, another instance of tc is running");
	    return(TN_FAILURE);
        }
    } else {
	sm.ErrorMessage("(TrigCoord::Startup): Unable to open/create LockFile");
	return(TN_FAILURE);
    }
    // Initialize the travel-time calculator
    if (tlay.ParseVelocityModel(velModelFile) == TN_FALSE) {
	sm.ErrorMessage("Error (TrigCoord::Startup): Unable to Parse VelocityModel model");
	return(TN_FAILURE);
    }
    
    // Register handler for TN_TMT_EVENT_SIG messages
    if (conn.registerHandler(TN_TMT_EVENT_SIG, handleEventSigMsg, 
			     (void *)this) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::Startup): Unable to register handler for TN_TMT_EVENT_SIG messages");
	return(TN_FAILURE);
    }

    // Register handler for TN_TMT_TRIGGER_SIG messages
    if (conn.registerHandler(TN_TMT_TRIGGER_SIG, handleTriggerSigMsg, 
			     (void *)this) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::Startup): Unable to register handler for TN_TMT_TRIGGER_SIG messages");
	return(TN_FAILURE);
    }

    // Set reliable delivery for all outgoing signal messages
    if (conn.setDeliveryMode(TN_TMT_EVENT_SIG, CONN_MODE_GMD) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::Startup): Unable to set delivery mode for TN_TMT_EVENT_SIG messages");
	return(TN_FAILURE);
    }

    // Subscribe to the event signal subject
    if (conn.subscribe(eventsubject) != TN_SUCCESS) {
	sm.ErrorMessage(Compat::Form("(TrigCoord::Startup): Unable to subscribe to %s", 
				     eventsubject));
	return(TN_FAILURE);
    }

    // Subscribe to the trigger signal subject
    if (conn.subscribe(trigsubject) != TN_SUCCESS) {
	sm.ErrorMessage(Compat::Form("(TrigCoord::Startup): Unable to subscribe to %s", 
				     trigsubject));
	return(TN_FAILURE);
    }

    sm.InfoMessage("(TrigCoord::Startup) Startup successful...");

    return(TN_SUCCESS);
}



int TrigCoord::Run()
{
    EventList::iterator elp;
    EventAssoc ea;
    TimeStamp curtime;
    Duration waittime;

    if (events.size() == 0) {
	sm.ErrorMessage("(TrigCoord::Run): Event list is empty");
	return(TN_FAILURE);
    }
  
    elp = events.begin();
    curtime = TimeStamp::current_time();
    ea = (*elp).second;

    // Incase we wake up before processing time (should not occur)
    if (curtime < (*elp).first) {
	nextwaketime = (*elp).first;
	return(TN_SUCCESS);
    } 

    // Remove this event from the queue
    events.erase(elp);
    switch (ea.curstate) {
    case TC_TRIG_ONLY:
	// Create a new event and associate it with this trigger
	if (this->_CreateEvent(ea.event, ea.trig) != TN_SUCCESS) {
	    sm.ErrorMessage("(TrigCoord::Run): Unable to create new event");
	} else {

	    // Send an associated event signal
	    if (this->_SendUnassocTrigSignal(ea.event.evid) != TN_SUCCESS) {
		sm.ErrorMessage("(TrigCoord::Run): Unable to send associated trigger signal");
	    }
	}
	sm.InfoMessage(Compat::Form("Purging trig %d from queue", 
				    ea.trig.trigid));
	break;
    case TC_EVENT_ONLY:
	sm.InfoMessage(Compat::Form("Purging event %d from queue", 
				    ea.event.evid));
	break;
    case TC_ASSOC:
	sm.InfoMessage(Compat::Form("Purging trig %d from queue", 
				    ea.trig.trigid));
	break;
    default:
	sm.ErrorMessage("(TrigCoord::Run): Invalid current state for event");
	return(TN_FAILURE);
	break;
    };

    // Set next wake-up time
    if (events.size() > 0) {
	elp = events.begin();
	nextwaketime = (*elp).first;
	waittime = Duration(nextwaketime - TimeStamp::current_time());
	sm.InfoMessage(Compat::Form("Next processing occurs in %.2lf secs", 
				    (double)waittime));
    } else {
	nextwaketime = PROGRAM_DEFAULT_WAKE;
    }
    _dumpQueue(); // DEBUG
  
    return(TN_SUCCESS);
}



int TrigCoord::Shutdown()
{
    sm.InfoMessage("(TrigCoord::Shutdown) Performing shutdown...");

    events.clear();
    checklist = 0;

    lock->unlock();
    delete lock;

    sm.InfoMessage("(TrigCoord::Shutdown) Shutdown successful...");

    return(TN_SUCCESS);
}


int TrigCoord::DoEvent(unsigned long evid)
{
    EventAssoc ea;
    EventList::iterator elp;
    Event e;
    Origin org;
    TimeStamp timeout;
    Duration waittime;
    int eventMatched = TN_FALSE;
    
    // Retrieve the event from the database
    e.evid = evid;
    if (this->_GetEvent(e) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::DoEvent): Unable to retrieve event from db");
	return(TN_FAILURE);
    }

    // Extract the event origin
    if (e.origins.size() == 0) {
	sm.ErrorMessage("(TrigCoord::DoEvent): Event has no origin");
	return(TN_FAILURE);
    }
    org = e.origins.front();

    sm.InfoMessage(Compat::Form("Checking trigs for matches with event %ld", evid));
    sm.InfoMessage(Compat::Form("Origin time %02d:%02d:%02d %02d/%02d/%04d", 
				org.origdate.hour_of_day(),
				org.origdate.minute_of_hour(),
				org.origdate.second_of_minute(),  
				org.origdate.day_of_month(),
				org.origdate.month_of_year(),
				org.origdate.year(),
				org.origdate.hour_of_day(),
				org.origdate.minute_of_hour(),
				org.origdate.second_of_minute(),  
				org.origdate.day_of_month(),
				org.origdate.month_of_year(),
				org.origdate.year()));

    // Check all unassociated triggers to see if event can be matched
    for (elp = events.begin(); elp != events.end(); elp++) {

	if ((*elp).second.curstate == TC_TRIG_ONLY ||
	    (*elp).second.curstate == TC_ASSOC) {
	    // Check if event origin time falls within start of trigger
	    sm.InfoMessage(Compat::Form("Comparing trig %ld, match time %02d:%02d:%02d %02d/%02d/%04d->%02d:%02d:%02d %02d/%02d/%04d",
					(*elp).second.trig.trigid,
					(*elp).second.matchtime.start.hour_of_day(),
					(*elp).second.matchtime.start.minute_of_hour(),
					(*elp).second.matchtime.start.second_of_minute(),  
					(*elp).second.matchtime.start.day_of_month(),
					(*elp).second.matchtime.start.month_of_year(),
					(*elp).second.matchtime.start.year(),
					(*elp).second.matchtime.end.hour_of_day(),
					(*elp).second.matchtime.end.minute_of_hour(),
					(*elp).second.matchtime.end.second_of_minute(),  
					(*elp).second.matchtime.end.day_of_month(),
					(*elp).second.matchtime.end.month_of_year(),
					(*elp).second.matchtime.end.year()));
	    
	    if ((org.origdate >= (*elp).second.matchtime.start) && 
		(org.origdate < (*elp).second.matchtime.end)) {
		
		int isGoodMatch = TN_FALSE;
		// Event is close in time; check how many station triggers are within 
		// the travel-time window. Skip this if the subnet trigger was already
		// associated with another binder event or if this binder event
		// was already matched with another subnet trigger.
		if ((*elp).second.curstate == TC_TRIG_ONLY && eventMatched == TN_FALSE) {
		    double matchPct = _GetTTMatchPct(e, (*elp).second.trig);
		    if (matchPct >= ttGoodMatchPct) {
			isGoodMatch = TN_TRUE;
			eventMatched = TN_TRUE;
			sm.InfoMessage(Compat::Form("Event can be associated by time with trigger %d and has %.2f percent TTwindow match: pass",
						    (*elp).second.trig.trigid, matchPct));
		    } else {
			sm.InfoMessage(Compat::Form("Event can be associated by time with trigger %ld but trigger but has only %.2f percent TTwindow match: fail",
						    (*elp).second.trig.trigid, matchPct));
		    }
		} else {
		    sm.InfoMessage(Compat::Form("Event can be associated by time with trigger %d which has already matched another event",
						(*elp).second.trig.trigid));
		}
		
		// Don't pull this subnet trigger off the queue; we want to see
		// if it matches other events.
		
		// Write the associations for this event
		if (this->_WriteAssocs(e, (*elp).second.trig, isGoodMatch) != TN_SUCCESS) {
		    sm.ErrorMessage("(TrigCoord::DoEvent): Unable to write assocs");
		    return(TN_FAILURE);
		}
		
		// Send an assoc_event signal if it matched.
		if (isGoodMatch == TN_TRUE) {
		    (*elp).second.event = e;
		    (*elp).second.curstate = TC_ASSOC;
		    if (this->_SendAssocEventSignal(e.evid) != TN_SUCCESS) {
			sm.ErrorMessage("(TrigCoord::DoEvent): Unable to send associated event signal");
			return(TN_FAILURE);
		    }
		}
	    
	    } else {
		// Event cannot be associated with this trigger
		sm.InfoMessage(Compat::Form("Event cannot be associated with trigger %ld",
					    (*elp).second.trig.trigid));
	    }
	}
    }

    if (eventMatched == TN_TRUE)
	return(TN_SUCCESS);

    // Insert this new event in the event list
    sm.InfoMessage(Compat::Form("Queueing event %ld for matching", e.evid));
    ea.event = e;
    ea.curstate = TC_EVENT_ONLY;
    timeout = org.origdate;
    timeout += maxtrigdur;
    timeout += maxprocdur;
    events.insert(EventList::value_type(timeout, ea));
  
    _dumpQueue();  // DEBUG  

    // Set next wake-up time
    elp = events.begin();
    nextwaketime = (*elp).first;
    waittime = Duration(nextwaketime - TimeStamp::current_time());
    sm.InfoMessage(Compat::Form("Next processing occurs in %.2lf secs", (double)waittime));

    return(TN_SUCCESS);
}


int TrigCoord::DoTrigger(unsigned long trigid)
{
    EventAssoc ea, eaMatch;
    EventList::iterator elp;
    NetworkTrigger trig;
    Origin org;
    TimeWindow matchtime;
    TimeWindow conttime;
    TimeStamp timeout;
    Duration waittime;
    TriggerList::iterator tlp;
    TimeStamp assocend;
    int trigMatched = TN_FALSE;

    // Retrieve the trigger from the database
    trig.trigs.clear();
    trig.trigid = trigid;
    if (this->_GetTrigger(trig) != TN_SUCCESS) {
	sm.ErrorMessage("(TrigCoord::DoTrigger): Unable to retrieve trigger from db");
	return(TN_FAILURE);
    }

    // Determine the time window over which this trigger can be matched
    // with an event.
    if (trig.trigs.size() == 0) {
	sm.ErrorMessage(Compat::Form("(TrigCoord::DoTrigger): No station triggers for this network trigger <%lu>", trigid));
	return(TN_FAILURE);
    }

    // Use the earliest save start and latest save end
    tlp = trig.trigs.begin();
    matchtime.start = (*tlp).second.savewin.start;
    matchtime.end = (*tlp).second.savewin.end;
    for (; tlp != trig.trigs.end(); tlp++) {
	if ((*tlp).second.savewin.start < matchtime.start) {
	    matchtime.start = (*tlp).second.savewin.start;
	}
	if ((*tlp).second.savewin.end > matchtime.end) {
	    matchtime.end = (*tlp).second.savewin.end;
	}
    }
    matchtime.start -= pretrigbuf;

    ea.trig = trig;
    ea.curstate = TC_TRIG_ONLY;
    ea.matchtime = matchtime;

    sm.InfoMessage(Compat::Form("Checking unassoc'd events for matches with trig %ld", trigid));
    sm.InfoMessage(Compat::Form("Match Time %02d:%02d:%02d %02d/%02d/%04d->%02d:%02d:%02d %02d/%02d/%04d",
				matchtime.start.hour_of_day(),
				matchtime.start.minute_of_hour(),
				matchtime.start.second_of_minute(),  
				matchtime.start.day_of_month(),
				matchtime.start.month_of_year(),
				matchtime.start.year(),
				matchtime.end.hour_of_day(),
				matchtime.end.minute_of_hour(),
				matchtime.end.second_of_minute(),  
				matchtime.end.day_of_month(),
				matchtime.end.month_of_year(),
				matchtime.end.year()));

    // Check all unassociated events to see if trigger can be matched
    // We may be removing elements from the events list (map), so we have
    // to be careful about the iterator elp!
    for (elp = events.begin(); elp != events.end();) {
	if ((*elp).second.curstate == TC_EVENT_ONLY) {
	    // Check if event origin time falls within start of trigger
	    org = (*elp).second.event.origins.front();

	    sm.InfoMessage(Compat::Form("Comparing event %ld, origin time %02d:%02d:%02d %02d/%02d/%04d", 
					(*elp).second.event.evid,
					org.origdate.hour_of_day(),
					org.origdate.minute_of_hour(),
					org.origdate.second_of_minute(),  
					org.origdate.day_of_month(),
					org.origdate.month_of_year(),
					org.origdate.year(),
					org.origdate.hour_of_day(),
					org.origdate.minute_of_hour(),
					org.origdate.second_of_minute(),  
					org.origdate.day_of_month(),
					org.origdate.month_of_year(),
					org.origdate.year()));

	    if ((org.origdate >= matchtime.start) && (org.origdate < matchtime.end)) {

		int isGoodMatch = TN_FALSE;
		// Event is close in time; check how many station triggers are within 
		// the travel-time window. Skip this if the subnet trigger was already
		// associated with another binder event.
		if (trigMatched == TN_FALSE) {
		    double matchPct = _GetTTMatchPct((*elp).second.event, trig);
		    if (matchPct >= ttGoodMatchPct) {
			isGoodMatch = TN_TRUE;
			trigMatched = TN_TRUE;
			sm.InfoMessage(Compat::Form("Trigger can be associated by time with event %ld  and has %.2f percent TTwindow match: pass", 
						    (*elp).second.event.evid, matchPct));
		    } else {
			sm.InfoMessage(Compat::Form("Trigger can be associated by time with event %ld  but has only %.2f percent TTwindow match: fail",
						    (*elp).second.event.evid, matchPct));
		    }
		} else {
		    sm.InfoMessage(Compat::Form("Trigger can be associated by time with event %ld; already matched another event", 
						(*elp).second.event.evid));
		}
		eaMatch = (*elp).second;
		
		// Pull this event out of the event list
		if (isGoodMatch) {
		    events.erase(elp++);
		}
		else {
		    ++elp;
		}
		
		// Write the associations for this event
		if (this->_WriteAssocs(eaMatch.event, trig, isGoodMatch) != TN_SUCCESS) {
		    sm.ErrorMessage("(TrigCoord::DoTrigger): Unable to write assocs");
		    return(TN_FAILURE);
		}

		// Send an event signal if it matched
		if (isGoodMatch == TN_TRUE) {
		    if (this->_SendAssocEventSignal(eaMatch.event.evid) != TN_SUCCESS) {
			sm.ErrorMessage("(TrigCoord::DoTrigger): Unable to send associated event signal");
			return(TN_FAILURE);
		    }
		    ea.curstate = TC_ASSOC;
		    ea.event = eaMatch.event;
		}
		
	    } else {
		// Trigger cannot be associated with this event
		sm.InfoMessage(Compat::Form("Trigger cannot be associated with event %ld",
					    (*elp).second.event.evid));
		++elp;
	    }
	} else {
	    ++elp;
	}
    }

    // Insert this new trigger in the trigger list
    // Even if it has already been matched to a binder event, we want
    // to see if it matches other binder events.
    sm.InfoMessage(Compat::Form("Queueing trigger %ld for matching", trig.trigid));
    timeout = trig.trigdate;
    timeout += assocdur;
    timeout += ecfinaldur;
    timeout += maxprocdur;
    events.insert(EventList::value_type(timeout, ea));

    _dumpQueue();  // DEBUG  

    // Set next wake-up time
    elp = events.begin();
    nextwaketime = (*elp).first;
    waittime = Duration(nextwaketime - TimeStamp::current_time());
    sm.InfoMessage(Compat::Form("Next processing occurs in %.2lf secs", (double)waittime));

    return(TN_SUCCESS);
}

double TrigCoord::_GetTTMatchPct(Event &e, NetworkTrigger &trig)
{
    int matched = 0;
    int trigChans = 0;    
    double pctMatch;
    TriggerList::iterator tlp;
    Origin org;
    
    if (e.origins.size() == 0) {
	sm.ErrorMessage("(TrigCoord::_getTTMatchPct): Event has no origin");
	return(TN_FAILURE);
    }
    org = e.origins.front();

    for (tlp = trig.trigs.begin(); tlp != trig.trigs.end(); tlp++) {
	if ((*tlp).second.trigflag != TRIG_TRIG) continue;
	trigChans++;
	double epi_dist = SeismoFuncs::GetDistanceKM(org.latitude, org.longitude,
						     (*tlp).first.latitude, (*tlp).first.longitude);
	double ptime = tlay.GetPWaveTravelTime(epi_dist, org.depth);
	double stime = tlay.GetSWaveTravelTime(epi_dist, org.depth);
	double trigTime = (*tlp).second.trigtime - org.origdate;
	
	sm.DebugMessage(Compat::Form("dist %.1f P %.1f S %.1f trig %.1f",
				    epi_dist, ptime, stime, trigTime));

	if (trigTime > ptime - ttPreP && trigTime < stime + ttPostS) matched++;
    }
    if (trigChans > 0) 
	pctMatch = 100.0 * (double)matched / (double)trigChans;

    return(pctMatch);
}

void TrigCoord::_dumpQueue()
{
    TimeStamp curtime = TimeStamp::current_time();
    Duration waittime;
    EventList::iterator elp;
    if (events.size() == 0) {
	sm.InfoMessage("Event queue is empty");
	return;
    }
    sm.InfoMessage("Event queue:");
    for (elp = events.begin(); elp != events.end(); elp++) {
	waittime = Duration((*elp).first - TimeStamp::current_time());
	if ((*elp).second.curstate == TC_EVENT_ONLY) {
	    sm.InfoMessage(Compat::Form("Event %ld in %.2lf secs",
					(*elp).second.event.evid,
					(double)waittime));
	}
	else if ((*elp).second.curstate == TC_TRIG_ONLY) {
	    sm.InfoMessage(Compat::Form("Trig %ld in %.2lf secs",
					(*elp).second.trig.trigid,
					(double)waittime));
	}
	else if ((*elp).second.curstate == TC_ASSOC) {
	    sm.InfoMessage(Compat::Form("Event %ld assoc with trig %ld in %.2lf secs",
					(*elp).second.event.evid,
					(*elp).second.trig.trigid,
					(double)waittime));
	} else {
	    sm.InfoMessage(Compat::Form("unexpected state %d in %.2lf secs",
					(*elp).second.curstate, 
					(double)waittime));
	}
    }
    return;
}
