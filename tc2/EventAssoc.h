/**
* @file
* @ingroup group_subnet_triggers_tc2
*/
/***********************************************************

File Name :
        EventAssoc.h

Original Author:
        Patrick Small

Description:


Creation Date:
        31 July 2000


Modification History:
	2013/03/21 Pete Lombard: Major changes and rename to tc2 


Usage Notes:


**********************************************************/

#ifndef event_assoc_H
#define event_assoc_H

// Various include files
#include "Event.h"
#include "NetworkTrigger.h"
#include "TimeWindow.h"


// The valid states than an unassociated event may be in
enum trigState {TC_ERROR, TC_TRIG_ONLY, TC_EVENT_ONLY, TC_ASSOC};


class EventAssoc
{
 private:

 public:
    trigState curstate;
    Event event;
    NetworkTrigger trig;
    TimeWindow matchtime;

    EventAssoc();
    EventAssoc(const EventAssoc &e);
    ~EventAssoc();

    EventAssoc& operator=(const EventAssoc &e);
};


#endif

