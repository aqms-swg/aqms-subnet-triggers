/**
* @file
* @ingroup group_subnet_triggers_tc2
*/
/***********************************************************

File Name :
        TriggerSignalHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_TRIGGER_SIG messages.


Creation Date:
        31 July 2000

Modification History:
	2013/03/21 Pete Lombard: Major changes and rename to tc2 


Usage Notes:


**********************************************************/

#ifndef trigger_signal_handler_H
#define trigger_signal_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Subnet Trigger message handler
void handleTriggerSigMsg(Message &m, void *arg);


#endif
