
# Master Makefile for Linux 64-bit

MAKE = make
MMACROS = 

SUBDIRS = ntrcg2 tc tc2 trig2db trig2ps

bin:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i;rm -f $$i; \
		${MAKE} ${MMACROS}; \
		cd ..; ); \
	done ;\

all:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i; \
		${MAKE} ${MMACROS}; \
		cd ..; ); \
	done ;\


clean:
	for i in ${SUBDIRS}; \
	do \
	(	echo "<<< Descending into directory: $$i >>>"; \
		cd $$i; \
		${MAKE} ${MMACROS} clean; \
		cd ..; ); \
	done ;\
