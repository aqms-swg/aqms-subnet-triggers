# tc2 {#man-tc2}  
  
## PAGE LAST UPDATED ON  
  
2022-02-11  
  
  
## NAME  
  
tc2  
  

## VERSION and STATUS  
  
???  
  
  
## PURPOSE  

The trigger coordinator receives inputs from the RT processing streams for binder events and subnet triggers. Ideally there would be a binder event for every subnet trigger. But the subnet trigger system is more sensitive for small earthquakes, so there may be subnet triggers with no binder event. If there is no binder event for a subnet trigger, then a new event is created with the limited information provided by the subnet trigger: trigger time and
the last of station triggers and their times; but no location or magnitude information.  
  

The old *tc* tried to match binder events and subnet triggers based only on the times of each. The new *tc2* compares the location and times of each station trigger with the P and S travel times of the binder event location. If enough of the trigger times fall within the P - S window, the subnet trigger becomes associated with the binder event; otherwise the subnet trigger becomes a new event. The other change is that when a subnet trigger becomes a new
event, its station triggers get promoted to zero-weight phase picks. This allows the event analysts to see what the subnet trigger system was doing.  
  
The reason for these changes is that in Northern California we have many small earthquakes in areas like the Geysers or Mammoth Lakes, sometimes while a larger earthquake happens somewhere else. The larger event would be detected by binder and the subnet trigger system, but the smaller event would only get a subnet trigger. Since the old tc was matching only based on the binder event and subnet trigger times, it would consider the events in two different locations to be the same event. That would mean that no record of the small
subnet trigger event would get into the database. The changes here and in [ntrcg2](man-ntrcg2.html) try to address these issues.  
  
#### Gory details  
  
**A description of what the code does**  

```  
On event signal arrival (DoEvent):
    Get Event info from database
    For all known EventAssocs in state Trig_Only or Assoc, find first event
    for which this new event falls within the TimeWindow "matchtime".
    if found {
        if EventAssoc is Trig_Only {
       	    get percent of triggers within TT window: P - preP 5, S + postS 10
	    if at least GoodMatchPercent 70% {
	        isGoodMatch = TRUE
		eventMatched = TRUE
            }
	}
	writeAssoc(isGoodMatch)   	
	Write association to database in AssocNtE table (evid, trigid, auth,
	  subsource, wfflag = isGoodMatch).
	if isGoodMatch {
	    Send AssocEvent signal	
	    associate new event with this trigger
        }
    }
    unless (eventMatched)
        Create new EventAssoc structure for this event.
	Set EventAssoc curstate = Event_Only
	set EventAssoc timeout = origin time + MaxTrigDuration + MaxProcDuration
	Insert (timeout, EventAssoc) pair into EventList
	Set tc wakeuptime to that of first EventAssoc on EventList
    }

On trigger signal arrival (DoTrigger):
    Get Trigger info from database NetTrig and trig_channel tables
    Set matchtime TimeWindow: start is earliest station trigger start -
      PreTrigBuf;
      end is earlier of latest station window end, matchtime.start + AssocDur
    For all events in state Event_Only, find events that fall within this
      trigger's matchtime window.
    If found && trigMatched == FALSE {
       	get percent of triggers within TT window: P - preP 5, S + postS 10
	if at least GoodMatchPercent 70% {
            isGoodMatch = TRUE
	    trigMatched = TRUE
	}
        if isGoodMatch Pull EventAssoc from EventList
	Write association to database in AssocNtE table (evid, trigid, auth,
	  subsource, wfflag = isGoodMatch).
	if isGoodMatch {
	    Send AssocEvent signal.
	    Associate trigger with binder event
        }
    }
    Create new EventAssoc structure for this trigger: see if this trigger
        associates with other events.
    Set EventAssoc curstate = Trig_Only
    set EventAssoc timeout = subnet trigger time + AssocDur + EcFinalDur +
        MaxProcDur 
     Insert (timeout, EventAssoc) pair into EventList
     Promote station triggers to phase picks with zero weight by writing to
        Arrival and AssocArO tables
     Set tc wakeuptime to that of first EventAssoc on EventList
    }

On EventAssoc timeout:
    Remove EventAssoc from EventList
    switch (curstate) {   no "drop-through" from one case to next.
    Trig_Only:
        Create event for this trigger {
	    Set Origin.locevid = trigger ID
	    Set Origin.timedate = trigger time
	    Set Origin location to bogus
	    Get new evid from database sequence
	    Write event info to database tables Event, Origin
	    Write association to database in AssocNtE table (evid, trigid, auth,
	      subsource, wfflag = TRUE).
	    Send UnassocTrig signal.
        }
    }
    set timeout for next event, if any.
```  
  

## HOW TO RUN   
  
tc2 is run using the following command line  
  
```
   tc2 tc2.cfg
```  
where the tc2.cfg is described below.  
  

## CONFIGURATION FILE   
  
tc2.cfg has the following arguments  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  
  
**LoggingLevel**  *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  
  
**ProgramName** *string*  
The unique name to allow this module to connect to CMS and be identifed.  
  
**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details. In addition, the common [CMS](cms.html) signals should be in the config file.    
  
**AssociationDuration** *integer*  
The number of seconds in which to associate an event with a subnet trigger.  
  
**MaxTrigDuration** *integer*  
The number of seconds to retain a subnet trigger in memory before releasing it that no matches showed up  
  
**ECFinalDuration** *integer*  
The time, in  seconds, it takes in seconds from origin time till the event coordinator releases the CMS signal for an event.  
  
**MaxProcDuration** *integer*  
The time it takes in seconds for any processes on an event to finish. This is added to the MaxTrigDuration and PreTriggerBuffer and the ECFinalDuration to create the end time of the subnet to event association matching.  
  
**PreTriggerBuffer** *integer*  
This is how many seconds before a subnet trigger to include in an event match.  
  
**TriggerSubject** *string*  
The name of the CMS signal to use for notification of Subnet Triggers (sent from trig2db).  
  
**EventSubject** *string*  
The name of the CMS signal to use for notification of Events (sent from trimag or ec as is done in NC).  
  
**AssocEventSubject** *string*  
The name of the CMS signal to use for events that have subnet triggers (usually received by ntrcg).  
  
**UnassocEventSubject** *string*  
The name of the CMS signal to use for events that have no subnet triggers (usually received by rcg).  
  
**HeartbeatSubject** *string*  
The heartbeat CMS signal that the module writes (note this is not used by any CMS receiver yet but is required).  
  
**UnassocTrigSubject** *string*  
The name of an unassociated trigger signal to be sent when a subnet trigger has no matching event. OPTIONAL.  

**Auth** *string*  
The 2 character network code of the authority running the tc module (e.g., CI, NC, BK, HI etc).  
  
**Subsource** *string*  
The RT host number of the host running TC so that different TC decisions can be attributed to different RT hosts.  
  
**HSInterval** *integer*  
How often in seconds to send a heartbeat signal to CMS.  
  
**VelocityModelFile** *string*  
file name of velocity model to be used for traceltime estimates.  
  
**ttGoodMatchPercent** *float*  
Percent of station triggers within traveltime window for subnet trigger to match binder event.  
  
**ttPreP** *integer*  h
The number of seconds for the station trigger match window before P arrival time.  
  
**ttPostS** *integer*  
The number of seconds for the station trigger match window after S arrival time.  
  
**promoteTriggersToArrivals** *integer*  
0 or 1 - whether or not to promote station triggers to phase arrivals.   
  
**DBService** *string*  
The name of the database being used.    
  
**DBUser** *string*  
The name of the database user account.  
  
**DBPasswd** *string*  
The password for the database user account specified by DBUser  
    

A sample tc2.cfg is shown below.  
  
```
#
# General configuration parameters
#
Include         common.cfg
Logfile         /home/ncss/run/logs/tc2
LockFile        /home/ncss/run/locks/tc.lock
LoggingLevel    2
ProgramName     TRIGCOORD
HSInterval      30


#
# Application configuration parameters
#
Auth                            NC
Subsource                       RTUCB2
TriggerSubject                  /signals/trig2db/trigger
EventSubject                    /signals/trimag/magnitude
AssocEventSubject               /signals/trigcoord/event/assoc
UnassocTrigSubject              /signals/trigcoord/event/unassoctrig

#
AssocDuration                   90
MaxTrigDuration                 500
# ECFinalDuration increased from 90 to 105 2006/12/13,04:00 UTC
# This is to allow for the delay of binder events waiting for codas
# plus delays for hypoinverse message to get processed by LoadREDI
# Revisit this setting if eqproc is changed or replaced.
# ECFinalDuration increased from 105 to 130 2007/12/16,17:40 UTC
ECFinalDuration                 130
MaxProcDuration                 15
PreTriggerBuffer                0
VelocityModelFile               /home/ncss/run/params/allcal_model.d
ttGoodMatchPercent              70
ttPreP                          5
ttPostS                         10
# Optional:
promoteTriggersToArrivals       1
#
#
Include                         commonDb.cfg
```  
  

## DEPENDENCIES  
  
The tc module depends on CMS being running and the Oracle Database being available and populated with the event and/or subnet trigger (nettrig) to be taken action on. Furthermore, the trig_channel table list of channels must match currently configured channels in the channel_data table. If there is no match from the trig_channel to channel_data table join, then there will be an error message in the tc log stating that there are NO STATION TRIGGERS found for a given trigid. This message is slightly misleading if you are unaware of
the join being performed.   
  
The triggers that tc ultimately handles get translated from the carlsubtrig earthworm module and it is critical that the CompAsWild flag NOT be set in the carlsubtrig.d configuration file. If it is set, then the trig_channel.seedchan field will have an asterisk in it and fail to do joins.  
  
Since it receives CMS signals from trig2db and ec, both of those programs must be operating as well.  

