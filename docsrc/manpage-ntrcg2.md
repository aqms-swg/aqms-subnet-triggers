# ntrcg2 {#man-ntrcg2}     
  
ntrcg2 stands for Network Trigger Request Card Generator 2.    
  
## PAGE LAST UPDATED ON  
  
2022-02-11  
  

## NAME  

ntrcg2  
  
  
## VERSION and STATUS  
  
???   
  
## PURPOSE  
  
Ntrcg2 receives event signals of two different subjects from tc2 (trigger coordinator). The contexts of these signals are either that a subnet trigger has been associated with a binder event; or that a subnet trigger event has been declared with no associated binder event. Ntrcg2 queries the database for the event and trigger information. Then it generates waveform request cards
for the appropriate channels and writes those to the database.   
  

#### Gory details  

**A description of what the source code does**   
     
On startup, Ntrcg2 processes any events left in its archive directory by calling DoEvent, described below.  
  
Throughout this text, 'SNCL' will be used to describe a particular data stream from a particular seismometer instrument. And 'channel' will refer to the 'C' part of SNCL, the SEED channel name. The term `data stream' will refer to the two letter character string that makes up the first part of a channel name; channel direction is the third letter, typically one of E, H or Z. This
terminology should reduce confusion. However, the names of data structures in ntrcg2 do not always follow this convention.  
  
On startup, a number of configuration parameters are read and data structures are set up before processing begins. Lists of 'configured' and 'active' SNCLs are read from the database. Configured SNCLs are listed in the AppChannels table as being configured for named programs; for ntrcg2 this is ProgramName in ntrcg2.conf. Each SNCL in AppChannels for ntrcg2 has a Mapfile specified for it. The Mapfile provides a mapping from a 'data stream' to several 'channel' names. The 'active' channels list comes from the channel_data table: ALL SNCLs listed in this table for current time.  
  
From the list of configured SNCLs, the channel names are mapped into larger lists of channel names and hence SNCLs. Those SNCLs which are found to be on the active list are formed into a ChannelSet for the configured SNCL; these are added to the ChannelMap.  
  
The idea for this channel mapping is as follows. Often the subnet trigger system is configured for one or a few of a station's channels. But we want to collect waveforms for all the channels of that station if that station triggers. The channel mapping provides this ability to map from the single channel of the station to all the channels of that station.  
  
On receipt of a CMS signal ntrcg2 checks the subject of the signal to see if it for an associated event or not. It then calls the DoEvent function.  
  
In DoEvent, ntrcg2 queries the database to determine if this machine should generate request cards: is this system in the master role or not. If not, no further action is taken on this event.  
  
```      
If this is an associated event {  
    query DB for event time, location, magnitude  
    if the magnitude is >includeAllMag (3.5, same as configured for rcg) {  
        no further action is needed by ntrcg2.  
   } else {  
      query DB for SNCLs of waveforms and request cards associated with this  
        event. Store in wfSNCLs list.  
      if no SNCLs are found {  
         set timer on this event for WFRetryInterval (60 sec)  
	 store event in archive directory (in case of restart)  
	 call DoEvent with isAssocEvent = TRUE  
      }  
      continue processing after end of this block  
   }  
}  
```  
    
Query DB for subnet trigger info including station triggers. Generate list of reuest cards:  
  
```
If the trigger.all_chans == TRUE {
   fill SelectedList with all configured channels. 
   Request priority is HIGH if mag >= 3.0; 
   else if channel has an arrival or a trigger, priority is MEDIUM; 
   else priority is LOW.
}
else {
   fill SelectedList with triggered channels and their mapped channels.
   If channel was not on our config list, use it anyway, without mapping.
}
```  
  
Remove event from archive if it is found there.  
  
Compare request card list with wfSNCLs list: any request card whose SNCL is in wfSNCLs has its request card removed, since the event already has a waveform or request card from rcg.  
  
The Selected list is dumped to a log file. All the requests are logged, and then written to the database waveform_request table for this event. On successful write to the database, a Request Signal is sent via CMS (contains evid only).
  

The big change here compared to the old ntrcg is this: for subnet triggers associated with binder events, ensure that request cards are made for all of the station triggers (and mapped channels) that were not already made by rcg for the binder event. This ensures that earthquakes detected only by the subnet trigger system at the same time as another event detected by binder will have the appropriate waveforms requested for human review.  
   

## HOW TO RUN  
    
ntrcg2 is run using the following command line:  
  
```
   ntrcg2 ntrcg2.cfg
```  
  
where the ntrcg2.cfg is described below.  
  

## CONFIGURATION FILE  
  
ntrcg2.cfg has the following arguments  
  
**CMSConfig** *string* cms.cfg  
Points to the cms configuration file which holds all CMS details. In addition, the common CMS signals should be in the config file.   
  
**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  
  
**LoggingLevel** *integer*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.   
  
**LockFile** *string*  
Filename with path. If you only wish one instance of this module to run, then the lockfile name should be unique per instance.  
  
**ProgramName** *string*  
The unique name to allow this module to connect to CMS and be identified. For e.g. RCG_NT  
  
**HSInterval** *integer*  
How often in seconds to send a heartbeat signal to CMS.  
  
**Auth** *string*  
Authority, the 2 character code of the authority running the ntrcg2 module (e.g., CI, NC, HI etc).   
  
**Subsource** *string*  
Specify the Source ID for ntrcg2 to insert into the database Subsource field of the request_card table.   
  
**EventSubject** *string*  
Specify the subject of CMS messages about new events that *[tc2](man-tc2.html)* has produced from subnet triggers.  
  
**AssocSubject** *string*  
Specify the subject of CMS messages about event associations between binder events and subnet triggers.  
  
**SignalSubject** *string*  
Specify the subject of the CMS message that ntrcg2 will publish upon the writing of request cards to the database.  
  
** ConfigDir** *string*  
Specify the directory in which ntrcg2 will look for 'map files' as specified in the config_channel database table. These files provide a mapping from the SNCLs in a subnet trigger to additional SNCLs that should have request cards generated for them.  
  
**EventLogDir** *string*  
Specify the directory in which ntrcg2 should create logs of the request cards generated for each event. Within 'log_dir', files will be named by event ID with a suffix of ".log".  
  
**IncludeAllMag** *float*  
Specify the magnitude above which [[rtem:rcg]]???(replace with ntrcg2???) generates request cards for all SNCLs.    
  
**WFRetryInterval** *integer*  
Specify the time, in seconds, ntrcg2 should wait for retrying an event after finding no waveforms or request cards in the database.  
  
**WFMaxRetryTime** *integer*  
Specify the time, in seconds, after which ntrcg2 should give up waiting for an event to have waveforms or request cards in the DB. Currently not used, but it probably should be added to ntrcg2.  
  
**DBService** *string*  
Specify the name of the database being used.  
  
**DBUser** *string*  
Specify the name of the database user account.  
  
**DBPasswd** *string*  
Specify the password for the database user account specified by DBUser.  
  
**DBConnectionRetryInterval** *integer*  
Specify the interval (in arbitrary units) for attempts to reconnect to the database. While this command is required, it has no effect on the behaviour of ntrcg2; more vapor-ware.  
  
**DBMaxConnectionRetries** *integer*  
Specify the number of times to attempt to reconnect to the database. While this command is required, it has no effect on the behavior of ntrcg2.  
  
An example configuration file used on ucbrt is shown below:  
  
```
#
# General configuration parameters
#
Include                 common.cfg
Logfile                 /home/ncss/run/logs/ntrcg2
LoggingLevel            2
LockFile                /home/ncss/run/locks/ntrcg.lock
ArchiveDirectory        /home/ncss/run/params/ntrcg2.state
ProgramName             RCG-NT
HSInterval              30


#
# Application configuration parameters
#
Auth                            NC
Subsource                       RTUCB2
EventSubject                    /signals/trigcoord/event/unassoctrig
AssocSubject                    /signals/trigcoord/event/assoc
SignalSubject                   /signals/ntrcg/event
ConfigDir                       /home/ncss/run/params/ntrcg
EventLogDir                     /home/ncss/run/logs/ntrcg
IncludeAllMag                   3.5
WFRetryInterval                 60
# WFMaxRetryTime is not currently used:
WFMaxRetryTime                  600
Include                         commonDb.cfg
DBConnectionRetryInterval       5
DBMaxConnectionRetries          5
</code>

An example mapping file used for most SNCLs on the NCSS system is shown here:
<code>
#
# Standard channel mapping file
#
# Don't leave trailing spaces after Channel names: the parser is too stupid
# to handle them!
#
# This is for mapping two-letter channel names to three-letter channel names
# for "standard" stations.
# The assumption here is that for stations with multiple sets of
# channels (HH? and BH?; or HH? and HN?) only one vertical out of these
# channels will be in the trigger list. The mapping here picks up the
# other channels in a given set without bringing in channels of
# different, unwanted, sets.
#
# Stations that SHOULD NOT be included here have some channels listed in
# channel_data but do not actually send those data to the RT system.
#
BH {
        Channel BHE
        Channel BHN
        Channel BHZ
        Channel BH1
        Channel BH2
}

EH {
        Channel EHE
        Channel EHN
        Channel EH1
        Channel EH2
        Channel EHZ
}

HH {
        Channel HH1
        Channel HH2
        Channel HH3
        Channel HHE
        Channel HHN
        Channel HHZ
}

SH {
        Channel SHE
        Channel SHN
        Channel SHZ
}

# DP covers BBEB,W02B,CMSB,CRQB,HERB,SM2B,RFSB; NOT BRIB
# Where present, CL?,DP[23] are triggered, BL?,BP[23] continuous
# Where present, CN? are continuous, so don't need BN?.
# DP1 is in carl*trig, continuous
# 2013/04/06: Added BG net's DP[ENZ] channels, although they are non-voting
#    in carl*trig.
DP {
        Channel BL1
        Channel BL2
        Channel BL3
        Channel BP2
        Channel BP3
        Channel CL1
        Channel CL2
        Channel CL3
        Channel CN1
        Channel CN2
        Channel CN3
        Channel DP1
        Channel DP2
        Channel DP3
        Channel DPE
        Channel DPN
        Channel DPZ
}

# EP covers MPBO stas, VALB plus MBS1,MDP1,MMX1
EP {
        Channel EP1
        Channel EP2
        Channel EP3
        Channel HL1
        Channel HL2
        Channel HL3
        Channel HN1
        Channel HN2
        Channel HN3
        Channel HS1
        Channel HS2
        Channel HS3
        Channel HV1
        Channel HDO
}

# HN covers CCH1,CGP1,CMW1,CSV1,CYD1
HN {
        Channel EP2
        Channel EP3
        Channel HN1
        Channel HN2
        Channel HN3
}
```
  

## DEPENDENCIES  
  
*  ntrcg2 uses the CMS system.  
  
* ntrcg2 depends on the program and config_channel tables for its channel configuration. The config_channel table may specify mapping files to be found in the ConfigDir given in the main configuration file.  
  
* ntrcg2 depends on the channel_data table for additional channels it may be configured to use.  
  
* ntrcg2 reads from the following database tables for trigger information: nettrig, trig_channel, origin, arrival, assocaro, assocnte.  
  
* ntrcg2 reads from the following database tables to determine if it is supposed to actually generate request cards: rt_role, peer_system_status  
  
* ntrcg2 writes its request card entries to the request_card.  
  
* Since carlsubtrig provides the subnet trigger information (through trig2ps and trig2db) that ntrcg2 uses, it is important that carlsubtrig be configured properly. The subnet lists for carlsubtrig must not contain wildcards. There is no way for any of carlsubtrig's downstream programs to fill in these wildcards with whatever SNCLs the operator may have intended.  
  

## MAINTENANCE    
  
* ntrcg2 needs to be restarted after the AppChannels table for it has been update. 
  
## BUG REPORTING  