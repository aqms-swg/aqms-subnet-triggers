# trig2ps {#man-trig2ps}

Trig2ps takes Earthworm TYPE_TRIGLIST_SCNL packets and writes the contents to a CMS XML message for trig2db to receive.

## PAGE LAST UPDATED ON

2020-04-10

## NAME

trig2ps

## VERSION and STATUS

v0.9.3 2013.01.17
status: ACTIVE

## PURPOSE

trig2ps takes Earthworm TYPE_TRIGLIST_SCNL packets and writes the contents to a CMS XML message for trig2db to receive. It is a full fledged Earthworm module even though it is coded in C++. Note that the TRIGLIST_SCNL messages come from the carlsubtrig module of Earthworm.

## HOW TO RUN
  
```
$ ./trig2ps
Usage: trig2ps <configfile>
version: 0.9.3 2013.01.17
```  
  
## CONFIGURATION FILE

The configuration file for trig2ps is named trig2ps.d and contains the following parameters. The configuration file in in Earthworm .d style (param value strings). Format of the description is :  

```
<parameter name> <data type> <default value>
<description>
```

**MyModuleId** *string* &nbsp;&nbsp; MOD_TRIG_PUB
Module id for this instance of the module.

**RingName** *string* &nbsp;&nbsp; SUBTRIG_RING  
Shared memory ring for input/output.  

**LogFile** *number*  
If 0, no logfile is generated. If 1, then a earthworm style logfile is created.  

**HeartBeatInterval** *number*  
The number of seconds between heartbeat messages for statmgr to monitor this module.  

**ProgramID** *string* &nbsp;&nbsp; SUBTRIG  
The id of the CMS module for being uniquely identified by the CMS system. If more than one trig2ps is running, then each should have its own ProgramID.

**List the message logos to grab from transport ring**

**GetHypsFrom** *string* &nbsp;&nbsp;INST_WILDCARD &nbsp;&nbsp;MOD_WILDCARD  
Allows the user to specify one or more of these lines to get particular modules or institutions carlsubtrig packets.  

**TssConfig** *string* 
Points to the cms configuration file, with path prefix, which holds all CMS details. In addition, the common CMS signals should be in the config file.  

**MsgSubject** *string*  
The name of the channel to publish the CMS message into for trig2ps to receive.  


*A sample configuration is shown below*  

```
#
# This is template's parameter file

#  Basic Earthworm setup:
#
MyModuleId         MOD_TRIG_PUB # module id for this instance of template 
RingName           SUBTRIG_RING # shared memory ring for input/output
LogFile            1            # 0 to completely turn off disk log file
HeartBeatInterval  30           # seconds between heartbeats
ProgramID          SUBTRIG
@db_ew_source.d
#
# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetHypsFrom   INST_CIT        MOD_WILDCARD    # archive message
#
# Talarian Smart Sockets configuration file
TssConfig       /app/aqms/run/cfg/cms/cms.cfg
MsgSubject      /parameters/trigger/carlsubtrig/subnettrig
```


## ENVIRONMENT

NA

## DEPENDENCIES

* The CMS system should be up and running and pointed to by this module. 
* This module only runs within a working Earthworm environment 7.X or greater version.  


## MAINTENANCE

Refer to the log file in the EW_LOG directory for details on the running and use sniffring on the SUBTRIG_RING to see if there are packets flowing. The receiver module, trig2db is a Real Time thread module and its log should be checked for reception of CMS messages.  

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-subnet-triggers/-/issues

## MORE INFORMATION

Look at the [Earthworm documentation](http://www.earthwormcentral.org/documentation4/index.html) for how an Earthworm module should behave. See trig2db manual page for what happens with the CMS message that this module generates.
