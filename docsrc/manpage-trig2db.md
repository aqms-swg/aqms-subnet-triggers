# trig2db {#man-trig2db}

Trig2db writes carlsubtrigger information from a CMS message into the database.

## PAGE LAST UPDATED ON

2020-04-10

## NAME

trig2db

## VERSION and STATUS

v2.2.2 2018-09-18
status: ACTIVE  

## PURPOSE

trig2db writes carlsubtrigger information from a CMS message into the database. The CMS message contains the carlsubtrig TRIGLIST_SCNL data and comes from the trig2ps Earthworm module. The module writes into 2 tables, the nettrig and trig_channel. Once the data are written to the database, then a CMS signal is initiated to the tc module.

## HOW TO RUN  
```
$ ./trig2db

usage: ./trig2db <config file>

version: 2.2.2 2018-09-18
DB:Using Oracle SQL syntax
```  



## CONFIGURATION FILE

The configuration file for trig2db is typically named trig2db.cfg and has the following arguments. Format for the description is  
```
<parameter name> <data type> <default value>
<description>
```  

**Include** *string*  
Include external files with path prefix.  

**TSSOption** *string*  
The cms configuration file, with path prefix,  which holds all CMS details. In addition, the common CMS signals should be in the config file.  
**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *number*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**LockFile** *string*  
The path to a lock file name to insure that only one instance is started.  

**ProgramName** *string* &nbsp;&nbsp; TrigToDB
The unique name to allow this module to connect to CMS and be identified.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  

**Application parameters **  


**TriggerSubject** *string*  
the name of the CMS signal to use for springing into action (sent from carlsubtrig).  

**SignalSubject** *string*  
The name of the CMS signal to use for notifying the next module (tc) that a network trigger has been written to the db.  

**Database connection parameters**


**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.

**DBPasswd** *string*  
The password for DBUser.  

*An example configuration file is shown below: *  

```
# Process template parameters

TSSOption       /app/aqms/run/cfg/cms/cms.cfg
Include         /app/aqms/run/cfg/cms/common_signals.cfg
Logfile         /app/aqms/run/logs/trig2db
LockFile        /app/aqms/run/locks/trig2db.lock
LoggingLevel    2
ProgramName     TrigToDB
HSInterval      30

# Application parameters

TriggerSubject                  /parameters/trigger/carlsubtrig/subnettrig
SignalSubject                   /signals/trig2db/trigger

# database params
Include /app/aqms/db/db_info.d
```  

where db_info.s is  

```
DBService       dbname  
DBUser          username  
DBPasswd        password  
```

## ENVIRONMENT

NA

## DEPENDENCIES

* aqms-libs  
* ACE TAO (third part libraries)  
* CMS should be running.
* The Oracle Database should be available. 
* Trig2ps should be running. trig2db relies on trig2ps for the CMS message.
* Carlsubtrig should be working. Trig2db takes carlsubtrig messages and converts them into Oracle SQL insert statements. As such, it cannot parse wild cards and CompAsWild should not be set in carlsubtrig.

## MAINTENANCE

No specific advice other than to check the log file and the database table for nettrig for writes.  

## BUG REPORTING

    https://gitlab.com/aqms-swg/aqms-subnet-triggers/-/issues

## MORE INFORMATION

trig2ps and aqms-subnet-triggers/tc
