# tc {#man-tc}

tc is an abbreviation for **Trigger Coordinator** and its job is to resolve coincident subnet triggers and events. 

## PAGE LAST UPDATED ON

2020-04-10

## NAME

tc

## VERSION and STATUS

v2.1.9 2018-12-19  
status: ACTIVE  

## PURPOSE

Resolve coincident subnet triggers and events. Tc launches CMS messages to the appropriate request card generator program which will write waveform requests for an event based trigger (hypoinverse) or subnet trigger (carlsubtrig). It also writes into the event table a "st" type event for subnet triggered only events. Tc is initiated by a CMS message from the trig2db module (Subnet Trigger Id into nettrig table) or from the ec module (Event Id into event table).

If there is only an event (no subnet trigger coincident), then the tc module issues a UnassocEvent CMS message. If there is ONLY a subnet trigger then the module will write an origin entry for the event and set the etype to "st" for Subnet Trigger and then send the UnassocTrigger CMS message. If there is a coincident event and subnet trigger, then the subnet trigger is associated with the event via the assocnte database table and the AssocEvent CMS signal is sent. The assocnte database table links the subnet trigger table nettrig with the event for which an association was made.  
  
The following pseudocode explains the work flow:

```
On event signal arrival:
    Get Event info from database
    For all known EventAssocs in state Trig_Only, find first event for which
    this new event falls within the TimeWindow "matchtime".
	if found, associate new event with this trigger: {
		Remove EventAssoc from EventList
		Set tc wakeuptime to that of first EventAssoc on EventList
		Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = TRUE).
		Send AssocEvent signal.
		For each EventAssoc in state Event_Only, find ones "contained" in this
			trigger. Contained means an event origin time falls within the
			EventAssoc's TimeWindow "conttime"
		If found {
			Remove this event from EventList
			Set tc wakeuptime to that of first EventAssoc on EventList
			Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = FALSE).
			no AssocEvent signal needed; it has already been sent(?)
		}
	}
	else {
		Create new EventAssoc structure for this event.
		Set EventAssoc curstate = Event_Only
		set EventAssoc timeout = origin time + MaxTrigDuration + MaxProcDuration
		Insert (timeout, EventAssoc) pair into EventList
		Set tc wakeuptime to that of first EventAssoc on EventList
	}  
	
	
On trigger signal arrival:
	Get Trigger info from database NetTrig and trig_channel tables
	Set matchtime TimeWindow: start is earliest station trigger start - PreTrigBuf; 
		end is earlier of latest station window end, matchtime.start + AssocDur
	Set conttime TimeWindow: start is earliest station trigger start - PreTrigBuf; 
		end is latest station window end time.
	For all events in state Event_Only, find events that fall within this
		trigger's matchtime window.
	If found {
		Pull EventAssoc from EventList
		Set tc wakeuptime to that of first EventAssoc on EventList
		Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = TRUE).
		Send AssocEvent signal.
		For each EventAssoc in state Event_Only, find ones "contained" in this
			trigger. Contained means an event origin time falls within the
			EventAssoc's TimeWindow "conttime"
		If found {
			    Remove this event from EventList
				Set tc wakeuptime to that of first EventAssoc on EventList
				Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = FALSE).
				no AssocEvent signal needed; it has already been sent(?)
				}
	}
	else {
		Create new EventAssoc structure for this trigger.
		Set EventAssoc curstate = Trig_Only
		set EventAssoc timeout = subnet trigger time + AssocDur + EcFinalDur + MaxProcDur 
		Insert (timeout, EventAssoc) pair into EventList
		Set tc wakeuptime to that of first EventAssoc on EventList
	}
			
			
On EventAssoc timeout:
    Remove EventAssoc from EventList
	switch (curstate) {   no "drop-through" from one case to next.
	Trig_Only:
		Create event for this trigger {
			Set Origin.locevid = trigger ID
			Set Origin.timedate = trigger time
			Set Origin location to bogus
			Get new evid from database sequence
			Write event info to database tables Event, Origin
			Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = TRUE).
			Send UnassocTrig signal.
			For each EventAssoc in state Event_Only, find ones "contained" in this
				trigger. Contained means an event origin time falls within the
				EventAssoc's TimeWindow "conttime"
			If found {
				Remove this event from EventList
				Set tc wakeuptime to that of first EventAssoc on EventList
				Write association to database in AssocNtE table (evid, trigid, auth, subsource, wfflag = FALSE).
				no AssocEvent signal needed; it has already been sent(?) 
		      }
		  }
	Event_Only:
		Send UnassocEvent Signal (event not associated with a trigger)
	}
```

## HOW TO RUN

```
$ ./tc

usage: ./tc <config file>

Version: 2.1.9 2018-12-19

DB: Using Oracle SQL syntax
```

## CONFIGURATION FILE

The configuration file for *tc* is named tc.cfg and is described below. Format is  
```
<parameter name> <data type> <default value>
<description>
```

**Include** *string*  
External file to be included, with path prefix.  

**TSSOption** *string*   
Points to the cms configuration file, with path prefix, which holds all CMS details. In addition, the common CMS signals should be in the config file.  

**Logfile** *string*  
The path with a logfile prefix where daily logs will be stored.  

**LoggingLevel** *string*  
Set the level of logging the program will do. 0 = off, higher numbers are more verbosity.  

**LockFile** *string*  
The path to a lock file name to ensure that only one instance is started. 


**ProgramName** *string* &nbsp;&nbsp;TRIGCOORD  
The unique name to allow this module to connect to CMS and be identifed.  

**HSInterval** *number*  
How often in seconds to send a heartbeat signal to CMS.  


**Application configuration parameters**  


**TriggerSubject** *string* &nbsp;&nbsp;signal_name  
The name of the CMS signal to use for notification of Subnet Triggers (sent from trig2db).  

**EventSubject** *string* &nbsp;&nbsp;signal_name  
The name of the CMS signal to use for notification of Events (sent from trimag or ec as is done in NC).  

**AssocEventSubject** *string* &nbsp;&nbsp;signal_name  
The name of the CMS signal to use for events that have subnet triggers (usually received by ntrcg).  

**UnassocEventSubject** *string* &nbsp;&nbsp;signal_name  
The name of the CMS signal to use for events that have no subnet triggers (usually received by rcg).  

**UnassocTrigSubject** *string* &nbsp;&nbsp;signal_name  
OPTIONAL. The name of an unassociated trigger signal to be sent when a subnet trigger has no matching event.  

**AssocDuration** *number*  
The number of seconds in which to associate an event with a subnet trigger.  

**MaxTrigDuration** *number*  
The number of seconds to retain a subnet trigger in memory before releasing it that no matches showed up (30 min at Caltech).  

**ECFinalDuration** *number*  
Number of seconds. Used to create the end time of the subnet to event association matching.  

**MaxProcDuration** *number*  
The time it takes in seconds for any processes on an event to finish.  

**PreTriggerBuffer** *number*  
The number of seconds before a subnet trigger to include in an event match.  


**Database connection parameters **  


**DBService** *string*  
The name of the database being used.  

**DBUser** *string*  
The name of the database user account.  

**DBPasswd** *string*  
The password for DBUser  

**DBConnectionRetryInterval** *number*  
Time to wait, in seconds, before re-attempting connection to the database.  

**DBMaxConnectionRetries** *number*  
Max number of times to attempt database connection.  

**Auth** *string*  
The 2 character network code of the authority running the tc module.  

**Subsource** *string*  
The RT host number of the host running TC so that different TC decisions can be attributed to different RT hosts.  


*A sample configuration file is shown below*  

```
#
# General configuration parameters
#
TSSOption       /app/aqms/run/cfg/cms/cms.cfg
Include         /app/aqms/run/cfg/cms/common_signals.cfg
Logfile         /app/aqms/run/logs/tc
LockFile        /app/aqms/run/locks/tc.lock
LoggingLevel    2
ProgramName     TRIGCOORD
HSInterval      30


#
# Application configuration parameters
#
TriggerSubject                  /signals/trig2db/trigger
EventSubject                    /signals/trimag/magnitude
EventSubject                    /signals/eventcoord/event
AssocEventSubject               /signals/trigcoord/event/assoc
UnassocEventSubject             /signals/trigcoord/event/unassoc
UnassocTrigSubject              /signals/trigcoord/event/unassoctrig
AssocDuration                   90
MaxTrigDuration                 1800
ECFinalDuration                 90
MaxProcDuration                 15
PreTriggerBuffer                15

# db related settings
Include /app/aqms/db/db_info.d
Include /app/aqms/db/db_source.d
```  

where db_info.d is  
```
DBService       dbname
DBUser          username
DBPasswd        password
```

and where db_source.d is  
```
# this is a commonly used fields in RT programs 
#
# to define the authority (netcode) for the program
Auth CI
#to define the subsource field in the database when writing data for a  program
Subsource  RT4
```



## ENVIRONMENT

NA

## DEPENDENCIES

* aqms-libs
* ACE_TAO (third party libraries)
* CMS must be running  
* Oracle or Postgres database should be available and populated with the event and/or subnet trigger (nettrig) to be taken action on. Furthermore, the trig_channel table list of channels must match currently configured channels in the channel_data table. If there is no match from the trig_channel to channel_data table join, then there will be an error message in the tc log stating that there are NO STATION TRIGGERS found for a given trigid. This message is slightly misleading if you are unaware of the join being performed.
* The triggers that tc ultimately handles get translated from the carlsubtrig earthworm module and it is critical that the CompAsWild flag NOT be set in the carlsubtrig.d configuration file. If it is set, then the trig_channel.seedchan field will have an asterisk in it and fail to do joins.
* Since it receives CMS signals from trig2db and ec, both of those programs must be operating as well.

## MAINTENANCE

No specific advice on tc. One thing to note is that if CompAsWild is used in the carlsubtrig, then tc will not work since this does not work in a SQL query that joins with the channel_data table.

## BUG REPORTING

https://gitlab.com/aqms-swg/aqms-subnet-triggers/-/issues

## MORE INFORMATION

trig2ps, aqms-event-cordinator, ntrcg, and rcg.
