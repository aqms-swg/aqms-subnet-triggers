/**                                                                                                                                                               
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file for TriggerSignalHandler.C
*/
/***********************************************************

File Name :
        TriggerSignalHandler.h

Original Author:
        Patrick Small

Description:

        This source file defines the interface to the call-back 
routine needed to process incoming TN_TMT_TRIGGER_SIG messages.


Creation Date:
        31 July 2000

Modification History:


Usage Notes:


**********************************************************/

#ifndef trigger_signal_handler_H
#define trigger_signal_handler_H


// Various include files
#include "Connection.h"


// Function prototype for Subnet Trigger message handler
void handleTriggerSigMsg(Message &m, void *arg);


#endif
