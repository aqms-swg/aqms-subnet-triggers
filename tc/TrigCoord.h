/**                                                                                                                                                               
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file for TrigCoord.h
*/
/***********************************************************

File Name :
        TrigCoord.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
	09 Dec 2016 - 64-bit system compliance updates performed

Usage Notes:


**********************************************************/

#ifndef trig2db_H
#define trig2db_H

// Various include files
#include <map>
#include <stdint.h>
#include "GenLimits.h"
#include "TimeStamp.h"
#include "Duration.h"
#include "Connection.h"
#include "RTApplication.h"
#include "EventAssoc.h"
#include "DatabaseTC.h"
#include "LockFile.h"

// Definition of the event list
typedef std::map<TimeStamp, EventAssoc, std::less<TimeStamp>, 
	std::allocator<std::pair<const TimeStamp, EventAssoc> > > EventList;


class TrigCoord : public RTApplication
{

 private:
    char auth[DB_MAX_AUTH_LEN+1];
    char subsource[DB_MAX_SUBSOURCE_LEN+1];
    char trigsubject[MAXSTR];
    char eventsubject[MAXSTR];
    char assocsigsubject[MAXSTR];
    char unassoceventsigsubject[MAXSTR];
    char unassoctrigsigsubject[MAXSTR];
    Duration assocdur;
    Duration maxtrigdur;
    Duration ecfinaldur;
    Duration maxprocdur;
    Duration pretrigbuf;
    char dbservice[MAXSTR];
    char dbuser[MAXSTR];
    char dbpass[MAXSTR];
    char dbschema[MAXSTR];
    int dbinterval;
    int dbretries;
    int checklist;
    char lock_file_name[MAXSTR];
    LockFile *lock;

    EventList events;

    int _GetDatabase(DatabaseTC &db);
    int _CreateEvent(Event &e, NetworkTrigger &trig);
    int _GetEvent(Event &e);
    int _GetTrigger(NetworkTrigger &t);
    int _WriteAssocs(Event &e, NetworkTrigger &trig, int wfflag);
    int _SendAssocEventSignal(uint32_t evid);
    int _SendUnassocEventSignal(uint32_t evid);
    int _SendUnassocTrigSignal(uint32_t evid);
    int _FindContained(NetworkTrigger &trig, TimeWindow conttime);

 protected:

 public:
    TrigCoord();
    ~TrigCoord();

    int ParseConfiguration(const char *tag, const char *value); /**< read configuration file */
    int Startup(); /**< Initialize trigger coordinator, including setting up handlers for CMS messages */
    int Run(); /**< Start the program, for both unassociated events and triggers */
    int Shutdown(); /**< Perform clean up and shutdown trigger coordinator */

    int DoEvent(uint32_t evid); /**< Associate provided event with matching unassociated triggers */
    int DoTrigger(uint32_t trigid); /**< Associate provided trigger with candidate events */
    
};

#endif
