/**
 * @file
 * @ingroup group_subnet_triggers
 * @brief Header file to DatabaseTC.C
 */
/***********************************************************

File Name :
        DatabaseTC.h

Original Author:
        Patrick Small

Description:


Creation Date:
        28 July 2000

Modification History:
			09 Dec 2016 - 64-bit compliance updates performed

Usage Notes:


**********************************************************/

#ifndef database_trig2db_H
#define database_trig2db_H

// Various include files
#include <stdint.h>
#include "Database.h"
#include "Event.h"
#include "NetworkTrigger.h"


class DatabaseTC : public Database
{
 private:

    int _IsPreferred();
    int _GetOrigin(uint32_t orid, Origin &org);
    int _WriteOrigin(Event &e, uint32_t &prefor);
    int _GetStationTriggers(NetworkTrigger &trig);

 protected:

 public:
    DatabaseTC();
    DatabaseTC(const char *dbs, const char *dbu, const char *dbp);
    ~DatabaseTC();

    int GetNextEventID(uint32_t &evid); /**< Get next event id from sequence */
    int GetEvent(Event &e); /**< Get preferred origin for event and associate it with event*/
    int GetTrigger(NetworkTrigger &trig); /**< Get subnet trigger information from nettrig */

    int WriteEvent(Event &e); /**< Insert event and origin information. Update event.prefor */
    int WriteSubnetComment(Event &e, NetworkTrigger &t); /**< Write a subnet network code into the remark table if it was a valid one */
    int AssocEventTrigger(Event &e, NetworkTrigger &t, char *network,
			  char *source, int wfflag); /**< Insert into association table assocnte, values from event and nettrig */
};


#endif
