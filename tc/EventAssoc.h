/**
 * @file
 * @ingroup group_subnet_triggers
 * @brief Class to track state of an association
 */
/***********************************************************

File Name :
        EventAssoc.h

Original Author:
        Patrick Small

Description:


Creation Date:
        31 July 2000


Modification History:


Usage Notes:


**********************************************************/

#ifndef event_assoc_H
#define event_assoc_H

// Various include files
#include "Event.h"
#include "NetworkTrigger.h"
#include "TimeWindow.h"


// The valid states than an unassociated event may be in
enum trigState {TC_ERROR, TC_TRIG_ONLY, TC_EVENT_ONLY, TC_ASSOC};


class EventAssoc
{
 private:

 public:
    trigState curstate;
    Event event;
    NetworkTrigger trig;
    TimeWindow matchtime;
    TimeWindow conttime;

    EventAssoc();
    EventAssoc(const EventAssoc &e);
    ~EventAssoc();

    EventAssoc& operator=(const EventAssoc &e);
};


#endif

