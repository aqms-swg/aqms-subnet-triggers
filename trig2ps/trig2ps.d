## @file
#  @ingroup group_subnet_triggers
#  @brief Earthworm style template config for trig2ps
#

#
# This is template's parameter file

#  Basic Earthworm setup:
#
MyModuleId         MOD_TRIG_PUB # module id for this instance of template 
RingName           SUBTRIG_RING # shared memory ring for input/output
LogFile            1            # 0 to completely turn off disk log file
HeartBeatInterval  30           # seconds between heartbeats
NetworkID	   CI
SourceID 	   RT3
ProgramID	   CARLSUBTRIG
#
# List the message logos to grab from transport ring
#              Installation       Module          Message Types
GetHypsFrom   INST_CIT        MOD_WILDCARD    # archive message
#
# CMS configuration file
CMSConfig	./cms.cfg
MsgSubject	/parameters/trigger/carlsubtrig/subnettrig
