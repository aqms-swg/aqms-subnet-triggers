:no_entry_sign: **CAUTION, WORK IN PROGRESS** :no_entry_sign:
# Subnet Triggers

This module processes subnet triggers.  

It consists of:  

* **tc** (trigger coordinator)
* **trig2db**
* **trig2ps**
* **tc2**  
* **ntrcg2**  
  
  
# TC (trigger coordinator)  

tc is an abbreviation for trigger coordinator and its job is to resolve coincident subnet triggers and events. Tc launches CMS messages to the appropriate request card generator program which will write waveform requests for an event based trigger (hypoinverse) or subnet trigger (carlsubtrig). It also writes into the event table a "st" type event for subnet triggered only events. Tc is initiated by a CMS message from the trig2db module (Subnet Trigger Id into nettrig table) or from the ec module (Event Id into event table).

If there is only an event (no subnet trigger coincident), then the tc module issues a UnassocEvent CMS message. If there is ONLY a subnet trigger then the module will write an origin entry for the event and set the etype to "st" for Subnet Trigger and then send the UnassocTrigger CMS message. If there is a coincident event and subnet trigger, then the subnet trigger is associated with the event via the assocnte database table and the AssocEvent CMS signal is sent. The assocnte database table links the subnet trigger table nettrig with the event for which an association was made.  
  
See man page for more details : [tc](man-tc.md)  
    
# Trig2db  

trig2db writes carlsubtrigger information from a CMS message into the database. The CMS message contains the carlsubtrig TRIGLIST_SCNL data and comes from the trig2ps Earthworm module. The module writes into 2 tables, the nettrig and trig_channel. Once the data are written to the database, then a CMS signal is initiated to the tc module.  
  
See man page for more details : [trig2db](man-trig2db.md)
  

# Trig2ps

trig2ps takes Earthworm TYPE_TRIGLIST_SCNL packets and writes the contents to a CMS XML message for trig2db to receive. It is a full fledged Earthworm module even though it is coded in C++. Note that the TRIGLIST_SCNL messages come from the carlsubtrig module of Earthworm.

See man page for more details : [trig2ps](man-trig2ps.md)  
  

# TC2 (Trigger Coordinator 2)  
  
*tc* tries to match binder events and subnet triggers based only on the times of each. The new *tc2* compares the location and times of each station trigger with the P and S travel times of the binder event location. If enough of the trigger times fall within the P - S window, the subnet trigger becomes associated with the binder event; otherwise the subnet trigger becomes a new event. The other change is that when a subnet trigger becomes a new event, its station triggers get promoted to zero-weight phase picks. This allows the event analysts to see what the subnet trigger system was doing.    
    
The reason for these changes is that in Northern California we have many small earthquakes in areas like the Geysers or Mammoth Lakes, sometimes while a larger earthquake happens somewhere else. The larger event would be detected by binder and the subnet trigger system, but the smaller event would only get a subnet trigger. Since the old tc was matching only based on the binder event and subnet trigger times, it would consider the events in two different locations to be the same event. That would mean that no record of the small subnet trigger event would get into the database. The changes here and in [ntrcg2](man-ntrcg2.md) try to address these issues.   

See man page for more details : [tc2](man-tc2.md)   
  

# ntrcg2  
  
*ntrcg2* receives event signals of two different subjects from *tc2* (trigger coordinator). The contexts of these signals are either that a subnet trigger has been associated with a binder event; or that a subnet trigger event has been declared with no associated binder event. *Ntrcg2* queries the database for the event and trigger information. Then it generates waveform request cards for the appropriate channels and writes those to the database.   
  
See man page for more details : [ntrgc2](man-ntrcg2)  





